'use strict';

var CHANGE_EVENT = 'change';
var dispatcher = require('../dispatcher');
var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');
var productMode = false;
var scrollPosition = false;
var location = {
	current: window.location.pathname,
	search: window.location.search,
	details: window.location,
	prev:'/',
	moduleName: null
};

var masterStore = assign({}, EventEmitter.prototype, {
	location: function(){ return location },
	client: function(){ return client },
	scrollPosition: function(){ return scrollPosition },
	emitChange: function() {
		this.emit(CHANGE_EVENT);
	},
	addChangeListener: function(callback) {
		this.on(CHANGE_EVENT, callback);
	},
	removeChangeListener: function(callback) {
		this.removeListener(CHANGE_EVENT, callback);
	}
});

masterStore.dispatchToken = dispatcher.register(function(action) {
	var fx = {
		CHANGE_LOCATION: function(){
			location.current = action.current;
			location.prev = action.prev;
			location.search = action.search;
			location.details = window.location;
			fx.WINDOW_CHANGE();
		},
		BACK_HISTORY: function(){
			var prev = location.prev;
			var next = location.current;
			location.prev = next;
			location.current = window.location.pathname;
			location.details = window.location;
			fx.WINDOW_CHANGE();
		},
		WINDOW_CHANGE: function(){
			var current = location.current.toLowerCase();
			location.moduleName = routes(current);
		},
		SCROLL_POSITION: function(){
			scrollPosition = action.position;
		},
		EDITION_MODE: function(){
			if(action.mode) editionMode = {
				mode: action.mode,
				id: action.id
			};
			else editionMode = null;
		}
	};

	if(fx[action.type]){
		var f = fx[action.type]();
		if(f === 'fxBreak') return;
		masterStore.emitChange();
	}
});

module.exports = masterStore;
