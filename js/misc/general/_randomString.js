'use strict';

global.randomString = function(x){
  return Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, x);
};
