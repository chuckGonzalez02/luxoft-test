Date.prototype.format = function(a) {
	var yyyy = this.getUTCFullYear().toString();
	var mm = ( this.getUTCMonth() + 1 ).toString();
	var dd = this.getUTCDate().toString();

	mm = (mm[1]?mm:"0"+mm[0]);
	dd = (dd[1]?dd:"0"+dd[0]);

	if(a){
		a = a.replace('yyyy', yyyy);
		a = a.replace('mm', mm);
		a = a.replace('dd', dd);

		return a;
	}
	else{
		return yyyy + '-' + mm + '-' + dd;
	}
};

global.customFormat = function(model, date){
  var h = completeTime( date.getHours() );
  var m = completeTime( date.getMinutes() );
  var s = completeTime( date.getSeconds() );
  var d = completeTime( date.getDate() );
  var M = completeTime( date.getMonth() + 1 );
  var y = completeTime( date.getFullYear() );

  return model.replace('HH', h).replace('mm', m).replace('SS', s).replace('DD', d).replace('MM', M).replace('YYYY', y);
};

global.completeTime = function(i) {
  i = Number(i);
  if (i < 10) i = '0' + i;
  return i;
};

global.getWeekDay = function(date){
  var index = date.getDay();
  return days[index];
};

global.days = ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'];
