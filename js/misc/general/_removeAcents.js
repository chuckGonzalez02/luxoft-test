'use strict';
String.prototype.removeAcents = function() {
    return this
    .replace(/á/g,'a')
    .replace(/é/g,'e')
    .replace(/í/g,'i')
    .replace(/ó/g,'o')
    .replace(/ú/g,'u')
	.replace(/Á/g,'A')
	.replace(/É/g,'E')
	.replace(/Í/g,'I')
	.replace(/Ó/g,'O')
	.replace(/Ú/g,'U')
};