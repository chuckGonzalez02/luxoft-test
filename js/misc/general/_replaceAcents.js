'use strict';
String.prototype.replaceAcents = function() {
    return this
    .replace(/á/g,'\u00e1')
    .replace(/é/g,'\u00e9')
    .replace(/í/g,'\u00ed')
    .replace(/ó/g,'\u00f3')
    .replace(/ú/g,'\u00fa')
	.replace(/Á/g,'\u00c1')
	.replace(/É/g,'\u00c9')
	.replace(/Í/g,'\u00cd')
	.replace(/Ó/g,'\u00d3')
	.replace(/Ú/g,'\u00da')
	.replace(/¿/g,'\u00bf')
	.replace(/ñ/g,'\u00f1')
	.replace(/Ñ/g,'\u00d1')
    .replace(/&#34;/g,'"')
    .replace(/Pulgadas/g,'"')
    .replace(/pulgadas/g,'"')
};