'use strict';

var masterStore = require('../../stores/masterStore');

window.addEventListener('resize', function() {
	var width = window.innerWidth;
	var height = window.innerHeight;

	if(client.os === 'iOS') width = screen.width, height = screen.height;

	if( !client.isMobileAgent ){
		if(width > 1200){
			client.headerType = 'desktop';
		}
		else if( width < 670 ){
			client.headerType = 'phone';
		}
		else{
			client.headerType = 'tablet';
		}
	}

	if(width < height){
		client.orientation = 'portrait';
	}
	else{
		client.orientation = 'landscape';
	}

	if(width < 880){
		client.size = 'dim';
	}
	else{
		client.size = 'agm';
	}

	masterStore.emitChange();
});
