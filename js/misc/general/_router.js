'use strict';

var masterActions = require('../../actions/masterActions');
var masterStore = require('../../stores/masterStore');

var router = function(a){
	if(typeof a === 'undefined'){
		return {
			host: window.location.hostname,
			path: window.location.pathname,
			port: window.location.port,
			protocol: window.location.protocol
		};
	}
	else{
		changeLocation(a);
	}
};

window.onpopstate = function(event) {
	backHistory();
};

global.changeLocation = function(a, b){
	var prev = router().path;
	var current = a;
	var search = b;
	masterActions.changeLocation(prev, current, search);
	window.history.pushState({url: masterStore.location().current}, null, masterStore.location().current);
	window.scrollTo(0, 0);
};

global.backHistory = function(){
	masterActions.backHistory();
};

global.routes = function(p){
	var path = p.toLowerCase();
	var pathArr = path.split('/');

	if(path === '/') return 'gateway';
	if(path === '/instapay') return 'instapay';
	if(path === '/recuperar') return 'recover';
	if(path === '/crear-tienda') return 'createStore';
	if(path === '/recupera-tu-password') return 'changePassword';
	if(path === '/home') return 'home';

	return 'default';
};
