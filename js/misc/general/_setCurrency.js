'use strict';
Number.prototype.setCurrency = function(a) {
    var re = '\\d(?=(\\d{' + (3) + '})+' + (2 > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~2)).replace(new RegExp(re, 'g'), '$&,');
};
