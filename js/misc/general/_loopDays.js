global.loopDays = function(ts, te, f, a){
	if(!a) var a = [];
	a.push(ts.format(f));

	if(ts.format(f) === te.format(f)){
		return a;
	}
	else{
		return loopDays(setDay(ts, 1), te, f, a);
	}
};
