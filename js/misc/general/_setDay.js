var month = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];

Date.prototype.format = function(wY) {
    var e = this.getMonth(), a = this.getDate().toString(), y = wY ? (' del ' + this.getFullYear()) : '';
    return (a[1] ? a : "0" + a[0]) + " de " + month[e] + y;
};

Date.prototype.customFormat = function(model){
  var h = completeTime( this.getHours() );
  var m = completeTime( this.getMinutes() );
  var s = completeTime( this.getSeconds() );
  var d = completeTime( this.getDate() );
  var M = completeTime( this.getMonth() + 1 );
  var y = completeTime( this.getFullYear() );

  return model.replace('HH', h).replace('mm', m).replace('SS', s).replace('DD', d).replace('MM', M).replace('MO', month[M - 1] ).replace('YYYY', y);
};

global.setDay = function(e) {
    var a = new Date
      , t = new Date(a);
    return t.setDate(a.getDate() + e),
    t.format()
};

var completeTime = function(i) {
  i = Number(i);
  if (i < 10) i = '0' + i;
  return i;
};
