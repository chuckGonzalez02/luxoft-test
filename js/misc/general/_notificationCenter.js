'use strict';

var socket = require('socket.io-client').connect();

var fxs = {};

socket.on('SEND_INVENTORY', function (data) {
  fxs[data.sku](data);
});

global._socket = {
  inventory: function(sku, domain, cb){
    fxs[sku] = cb;
    socket.emit('GET_INVENTORY', {
      sku: sku,
      domain: domain
    });
  }
};
