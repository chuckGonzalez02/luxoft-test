'use strict';

var masterActions = require('../../actions/masterActions');

var $swipe = function(){
	var MOVE_BUFFER_RADIUS = 10;
    var POINTER_EVENTS = {
        'mouse': {
            start: 'mousedown',
            move: 'mousemove',
            end: 'mouseup'
        },
        'touch': {
            start: 'touchstart',
            move: 'touchmove',
            end: 'touchend',
            cancel: 'touchcancel'
        }
    };

    function getCoordinates(event) {
        var originalEvent = event.originalEvent || event;
        var touches = originalEvent.touches && originalEvent.touches.length ? originalEvent.touches : [originalEvent];
        var e = (originalEvent.changedTouches && originalEvent.changedTouches[0]) || touches[0];

        return {
            x: e.clientX,
            y: e.clientY
        };
    }

    function getEvents(pointerTypes, eventType) {
        var res = [];
        map(pointerTypes, function(pointerType) {
            var eventName = POINTER_EVENTS[pointerType][eventType];
            if (eventName) {
                res.push(eventName);
            }
        });
        return res.join(' ');
    }

	return {
		bind: function(element, eventHandlers, pointerTypes) {
			var totalX, totalY;
			var startCoords;
			var lastPos;
			var active = false;

			pointerTypes = pointerTypes || ['mouse', 'touch'];

			element.ontouchstart = function(event) {
				startCoords = getCoordinates(event);
				active = true;
				totalX = 0;
				totalY = 0;
				lastPos = startCoords;
				eventHandlers['start'] && eventHandlers['start'](startCoords, event);
			};

			var events = getEvents(pointerTypes, 'cancel');

			if (events) {
				element.ontouchcancel = function(event) {
					active = false;
					eventHandlers['cancel'] && eventHandlers['cancel'](event);
				};
			}

			element.ontouchmove = function(event) {
				if (!active) return;
				if (!startCoords) return;

				var coords = getCoordinates(event);
				totalX += Math.abs(coords.x - lastPos.x);
				totalY += Math.abs(coords.y - lastPos.y);

				lastPos = coords;
				if (totalX < MOVE_BUFFER_RADIUS && totalY < MOVE_BUFFER_RADIUS) {
					return;
				}

				if (totalY > totalX) {
					active = false;
					eventHandlers['cancel'] && eventHandlers['cancel'](event);
					return;
				} else {
					event.preventDefault();
					eventHandlers['move'] && eventHandlers['move'](coords, event);
				}
			};

			element.ontouchend = function(event) {
				if (!active) return;
				active = false;
				eventHandlers['end'] && eventHandlers['end'](getCoordinates(event), event);
			};
		}
	};
};

function makeSwipeDirective(directiveName, direction, eventName) {
	var MAX_VERTICAL_DISTANCE = 75;
	var MAX_VERTICAL_RATIO = 0.3;
	var MIN_HORIZONTAL_DISTANCE = 30;

    return function(element) {
		//var swipeHandler = $parse(attr[directiveName]);
		var startCoords, valid;

		function validSwipe(coords) {
	        if (!startCoords) return false;
	        var deltaY = Math.abs(coords.y - startCoords.y);
	        var deltaX = (coords.x - startCoords.x) * direction;
	        return valid &&
	            deltaY < MAX_VERTICAL_DISTANCE &&
	            deltaX > 0 &&
	            deltaX > MIN_HORIZONTAL_DISTANCE &&
	            deltaY / deltaX < MAX_VERTICAL_RATIO;
		}

		var pointerTypes = ['touch'];

		$swipe().bind(
			element,
			{
				'start': function(coords, event) {
					startCoords = coords;
					valid = true;
				},
				'cancel': function(event) {
					valid = false;
				},
				'end': function(coords, event) {
					if (validSwipe(coords)) {
						masterActions.swipe(direction);
					}
				}
			}, pointerTypes);
    };
}

var swipeleft = makeSwipeDirective('ngSwipeLeft', -1, 'swipeleft');
var swiperight = makeSwipeDirective('ngSwipeRight', 1, 'swiperight');

var page = document.getElementById('page');

swipeleft(page), swiperight(page);
