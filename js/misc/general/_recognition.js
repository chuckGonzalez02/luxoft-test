'use strict';

global.recognition = function(cb, end_cb){
  var r = new webkitSpeechRecognition();
  var getFinal = false;
  //r.continuous = true;
  r.interimResults = true;
  r.lang = "es";
  r.onresult = function(event){
    var interim_transcript = '';
    var final_transcript = '';

    for (var i = event.resultIndex; i < event.results.length; ++i) {
      if (event.results[i].isFinal) {
        final_transcript += event.results[i][0].transcript;
      } else {
        interim_transcript += event.results[i][0].transcript;
      }
    }
    if(final_transcript) getFinal = true;
    cb(interim_transcript, final_transcript);
  };

  r.onend = function(event){
    if(!!end_cb && !getFinal) end_cb();
  };

  r.start();

  return r;
};
