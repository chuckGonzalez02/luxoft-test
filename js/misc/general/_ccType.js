window.ccType = function(accountNumber) {
	var result = "unknown";
	if (/^5[1-5]/.test(accountNumber)) {
		result = "mastercard";
	}
	else if (/^4/.test(accountNumber)) {
		result = "visa";
	}
	else if (/^3[47]/.test(accountNumber)) {
		result = "amex";
	}
	return result;
};
