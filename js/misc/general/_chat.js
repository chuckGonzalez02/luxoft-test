'use strict';

var customFormat = function(model, date){
  var h = completeTime( date.getHours() );
  var m = completeTime( date.getMinutes() );
  var s = completeTime( date.getSeconds() );
  var d = completeTime( date.getDate() );
  var M = completeTime( date.getMonth() + 1 );
  var y = completeTime( date.getFullYear() );

  return model.replace('HH', h).replace('mm', m).replace('SS', s).replace('DD', d).replace('MM', M).replace('YYYY', y);
};

var completeTime = function(i) {
  i = Number(i);
  if (i < 10) i = '0' + i;
  return i;
};

module.exports = function(){
  var _this = this;
  var hasRoom = false;
  var hasAgent = false;
  var fg = adminCookie.get('pcx');
	var pcx = adminCookie.get('pcx');

  this.ref = firebase.database().ref('LOCOMPROMX/CHAT/');
  this.msgs = [];

  this.agentsRef = this.ref.child('agents');
  this.roomsRef = this.ref.child('rooms/' + fg );
  this.msgRef = this.ref.child('msgs/' + fg );

  this.agents = {};
  this.emitAgentChange = null;

  this.onAgentsChange = function(callback){
    this.agentsRef.orderByValue().limitToLast(1).on('value', function(s) {
      _this.agents = s.val();
      callback(_this.agents);
    });
  };

  this.sendMessage = function(msg){
    if(!hasRoom) buildRoom();

    var d = new Date();

    _this.msgRef.push({
      stamp: d.toString(),
      sender: fg,
      msg: msg
    });
  };

  this.listenMsgs = function(cb){
    this.msgRef.on('child_added', function(data){
			var el = data.val();

      el.hourData = (function(){
        var now = new Date(el.stamp);
        return customFormat('HH:mm', now);
      })();

      _this.msgs.push(el);

      cb(_this.msgs);
		});
  };

  var buildRoom = function(){
    _this.roomsRef.transaction(function(userRoom){
      if(!userRoom) userRoom = {
				agent: false,
				pcx: pcx
			};
      return userRoom;
    });
  };
};
