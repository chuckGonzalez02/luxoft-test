'use strict';

global.pluralize = function(a){
  var n = Number(a);
  var isNumber = !isNaN(n);

  if(isNumber){
    if(n > 1) return 's';
    else return '';
  }
  else{
    return '';
  }
};
