'use strict';

var React = require('react');
var ReactDom = require('react-dom');
var BottomNavigationParent = require('material-ui/BottomNavigation');

global.div = React.DOM.div;
global.h1 = React.DOM.h1;
global.h2 = React.DOM.h2;
global.h3 = React.DOM.h3;
global.p = React.DOM.p;
global.RawA = React.DOM.a;
global.ul = React.DOM.ul;
global.li = React.DOM.li;
global.ol = React.DOM.ol;
global.span = React.DOM.span;
global.img = React.DOM.img;
global.input = React.DOM.input;
global.form = React.DOM.form;
global.br = React.DOM.br;
global.i = React.DOM.i;
global.textarea = React.DOM.textarea;
global.select = React.DOM.select;
global.option = React.DOM.option;
global.meta = React.DOM.meta;
global.pre = React.DOM.pre;
global.iframe = React.DOM.iframe;
global.canvas = React.DOM.canvas;
global.RawA = React.DOM.a;
global.button = React.DOM.button;
global.strong = React.DOM.strong;
global.fieldset = React.DOM.fieldset;
global.label = React.DOM.label;
global.video = React.DOM.video;
global.header = React.DOM.header;
global.footer = React.DOM.footer;
global.table = React.DOM.table;
global.tbody = React.DOM.tbody;
global.tr = React.DOM.tr;
global.td = React.DOM.td;
global.path = React.DOM.path;
global.main = React.DOM.main;
global.nav = React.DOM.nav;
global.section = React.DOM.section;
global.svg = React.DOM.svg;
global.polygon = React.DOM.polygon;
global.nav = React.DOM.nav;
global.h4 = React.DOM.h4;
global.__Frame = React.createClass({
  render: function() {
    return iframe({
      style: this.props.style
    });
  },
  componentDidMount: function() {
    this.renderFrameContents();
  },
  renderFrameContents: function() {
    var doc = ReactDom.findDOMNode(this).contentDocument;
    if(doc.readyState === 'complete') {
			var contents = div(null, this.props.children);
			ReactDOM.render(this.props.children, doc.body);
    } else {
       setTimeout(this.renderFrameContents, 0);
    }
  },
  componentDidUpdate: function() {
    this.renderFrameContents();
  },
  componentWillUnmount: function() {
    ReactDom.unmountComponentAtNode(ReactDom.findDOMNode(this).contentDocument.body);
  }
});

global.Frame = function(){
	return createComponent(__Frame, arguments);
};
global.a = function(){
	var args = Array.prototype.slice.call(arguments);
	var params = null;
	var els = [];
	var arr = [];
	if(args.length > 0){
		params = args[0] || {};
		var href = params.href;
		var callback = params.onClick;
		params.onClick = function(event){
			if( typeof callback === 'function') var r = callback(event);
			if((params && params.href && params.href.indexOf('//') === 0 || params && params.href && params.href.indexOf('http') === 0) && (!r || r !== 'fxBreak') ){

			}
			else if( params && params.href ){
				if(!r || r !== 'fxBreak'){
					event.preventDefault();
					changeLocation(href, '');
				}
			}
		};
	}
	if(args.length > 1){
		args.shift();
		els = args;
	}
	arr.push(params);
	map(els,function(val,key){
		arr.push( val );
	});
	return RawA.apply(this, arr);
};
global.SvgIcon = function(){
	return createComponent(material.SvgIcon, arguments);
};
global.Paper = function(){
	return createComponent(material.Paper, arguments);
};
global.TextField = function(){
	return createComponent(material.TextField, arguments);
};
global.SelectField = function(){
	return createComponent(material.SelectField, arguments);
};
global.MenuItem = function(){
	return createComponent(material.MenuItem, arguments);
};
global.RaisedButton = function(){
	return createComponent(material.RaisedButton, arguments);
};
global.FlatButton = function(){
	return createComponent(material.FlatButton, arguments);
};
global.Dialog = function(){
	return createComponent(material.Dialog, arguments);
};
global.Snackbar = function(){
	return createComponent(material.Snackbar, arguments);
};
global.Stepper = function(){
	return createComponent(material.Stepper, arguments);
};
global.Step = function(){
	return createComponent(material.Step, arguments);
};
global.StepContent = function(){
	return createComponent(material.StepContent, arguments);
};
global.StepLabel = function(){
	return createComponent(material.StepLabel, arguments);
};
global.StepButton = function(){
	return createComponent(material.StepButton, arguments);
};
global.DatePicker = function(){
	return createComponent(material.DatePicker, arguments);
};
global.AutoComplete = function(){
	return createComponent(material.AutoComplete, arguments);
};
global.Card = function(){
	return createComponent(material.Card, arguments);
};
global.CardMedia = function(){
	return createComponent(material.CardMedia, arguments);
};
global.CardHeader = function(){
	return createComponent(material.CardHeader, arguments);
};
global.CardText = function(){
	return createComponent(material.CardText, arguments);
};
global.CardTitle = function(){
	return createComponent(material.CardTitle, arguments);
};
global.CardActions = function(){
	return createComponent(material.CardActions, arguments);
};
global.Toggle = function(){
	return createComponent(material.Toggle, arguments);
};
global.Slider = function(){
  return createComponent(material.Slider, arguments);
};
global.BottomNavigation = function(){
  return createComponent(BottomNavigationParent.BottomNavigation, arguments);
};
global.BottomNavigationItem = function(){
  return createComponent(BottomNavigationParent.BottomNavigationItem, arguments);
};

var createComponent = function(element, args){
	args = Array.prototype.slice.call(args) || [];
	args.splice(0, 0, element);
	return React.createElement.apply(this, args);
};
