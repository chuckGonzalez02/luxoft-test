'use strict';

var dispatcher = require('../dispatcher');

module.exports = {
	changeLocation: function(prev, current, search) {
		dispatcher.dispatch({
			type: 'CHANGE_LOCATION',
			prev: prev,
			current: current,
			search: search
    });
	},
	backHistory: function(){
		dispatcher.dispatch({
			type: 'BACK_HISTORY'
		});
	},
	swipe: function(direction){
		dispatcher.dispatch({
			type: 'SWIPE',
			direction: direction
		});
	},
	scrollPosition: function(position) {
		dispatcher.dispatch({
			type: 'SCROLL_POSITION',
			position: position
    	});
	},
	dockerModule: function(obj){
		dispatcher.dispatch({
			type: 'DOCKER_MODULE',
			obj: obj
    });
	},
	editionMode: function(mode, id){
		dispatcher.dispatch({
			type: 'EDITION_MODE',
			mode: mode,
			id: id
    });
	}
};
