'use strict';

const urlBase = 'https://redcube.io/api/webservice?webservice=products&botToken=5566f770-2f50-11e7-919e-cb84a8a5e996';
const ProductElement = require('./_ProductElement');

module.exports = class MasterComponent extends _ReactComponent {
	constructor(props) {
    super(props);

    this.fetchProducts = this.fetchProducts.bind(this);
    this.changePage = this.changePage.bind(this);
		this.changeSort = this.changeSort.bind(this);
		this.changeDirection = this.changeDirection.bind(this);
		this.handleStandardChange = this.handleStandardChange.bind(this);

    this.state = {
      total: 0,
      offset: 0,
      loading: true,
      hits: [],
			sort: '_score',
			direction: 'desc',
			sortFE: '0',
			directionFE: 'desc'
    };
  }
  componentDidMount() {
    this.fetchProducts();
  }
  fetchProducts() {
    var state = this.state;
    var uri = '';

    uri += urlBase;
    uri += `&offset=${state.offset}`;
    uri += '&size=100';
    uri += `&sortProperty=${state.sort}&sortValue=${state.direction}`;

    this.setState({loading: true});

    GET(uri, (data) => {
      if(data && data.hits && data.hits.hits) {
        this.setState({
          total: data.hits.total || data.hits.hits.length || 0,
          hits: data.hits.hits,
          loading: false
        });
      }
    });
  }
  changePage(a) {
    var state = this.state;
		var whereiam = state.offset;
		var qty = 100;
		var count = state.total;

		if(a === 0){
			whereiam = whereiam - qty;
			if(whereiam < 0) whereiam = 0;
		}
		else {
			whereiam = whereiam + qty;
			if( whereiam > (count - qty) ) whereiam = count - qty;
			if(whereiam < 0) whereiam = 0;
		}

		this.setState({ offset: whereiam });
  }
	changeSort(e) {
		let sort = e.target.value;
		return this.setState({sort});
	}
	changeDirection(e) {
		let direction = e.target.value;
		return this.setState({direction});
	}
  componentDidUpdate(prevProps, prevState) {
    const offset = prevState.offset;
    const newOffset = this.state.offset;

		const sort = prevState.sort;
		const newSort = this.state.sort;

		const direction = prevState.direction;
		const newDirection = this.state.direction;

    if(
			offset !== newOffset ||
			sort !== newSort ||
			direction !== newDirection
		) this.fetchProducts();
  }
	handleStandardChange(e) {
		const newState = {};

		newState[e.target.name] = e.target.value;

		this.setState(newState);
	}
	render() {
		const state = this.state;
		var hits = state.hits.concat([]);

		if(state.sortFE === 'price') {
			hits.sort(function(a, b) {
				let v = -1;
				if(state.directionFE === 'asc') v = 1;
				if(a._source.commerce.price > b._source.commerce.price) return v;
				else return v * -1;
				return 0;
			});
		}

		return section({},
			header(null,
				h1(null, 'Productos'),
				div({id: 'Paginator'},
					span({
            onClick: () => this.changePage(0)
          }, 'Anterior'),
					span(null, `${state.offset} - ${state.offset + state.hits.length} de ${state.total}`),
					span({
            onClick: () => this.changePage(1)
          }, 'Siguiente')
				)
			),
			div({className: 'sortBar'},
				select({className: 'sort', value: state.sort, onChange: this.changeSort},
					option({value: '_score'}, 'Relevancia (Backend)'),
					option({value: 'commerce.price'}, 'Precio (Backend)')
				),
				select({className: 'sort', value: state.direction, onChange: this.changeDirection},
					option({value: 'asc'}, 'Ascendente (Backend)'),
					option({value: 'desc'}, 'Descendente (Backend)')
				)
			),
			div({className: 'sortBar'},
				select({className: 'sort', name: 'sortFE', value: state.sortFE, onChange: this.handleStandardChange},
					option({value: '0'}, 'Ninguna (Frontend)'),
					option({value: 'price'}, 'Precio (Frontend)')
				),
				select({className: 'sort', name: 'directionFE', value: state.directionFE, onChange: this.handleStandardChange},
					option({value: 'asc'}, 'Ascendente (Frontend)'),
					option({value: 'desc'}, 'Descendente (Frontend)')
				)
			),
			main({className: 'card'},
				state.loading ?
					div({className: 'loader'}, 'Cargando') :
					map(hits, (val, key) => _createElement(ProductElement, {
	          	data: val._source || null,
	          	key: val._id
	        	})
					)
			)
		);
	}
};
