'use strict';

module.exports = class Product extends _ReactComponent{
  constructor(props) {
    super(props)
  }
  render() {
    const data = this.props.data;
    if(!data) return null;

    let images = data.productImages || {};
    let content = data.content;
		let image = images['0'] || '';
		let src;

		if(!image) src = '';
		else src = image.replace('https://firebasestorage.googleapis.com', 'https://locompro.com') + '&size=70';

    return div({className: 'element'},
      img({ src }),
      div({className: 'data'},
        h2(null, content.name || ''),
        div({className: 'upc'}, `${data.id} - $${data.commerce.price}`)
      )
    );
  }
}
