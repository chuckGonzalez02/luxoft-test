'use strict';

require('./misc');

var React = require('react');

global._createElement = React.createElement;
global._createClass = React.createClass;
global._ReactComponent = React.Component;
global.ReactCSSTransitionGroup = require('react/lib/ReactCSSTransitionGroup');
global.assign = Object.assign;

var Master = require('./components/master.react');

require('react-dom').render(
  React.createElement(Master),
  document.getElementById('page')
);
